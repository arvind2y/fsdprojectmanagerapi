﻿using AutoMapper;
using NUnit.Framework;
using ProjectManager.Logic.Tests.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using dbEntity= ProjectManager.DataAccess.Entity;
using boObject = ProjectManager.BusinessObjects;

namespace ProjectManager.Logic.Tests.Mappings
{
    [TestFixture]
    public class ProjectMapsTests
    {
        private IMapper _mapper;
        [SetUp]
        public void Setup()
        {
            _mapper = UnityUlitity.SetupAutoMapper();
        }

        [Test]
        public void ProjectMaps_ProjectDbEntityToBusinessObject()
        {
            var entity = new dbEntity.Project
            {
                Project_Id = 101,
                Project_Name = "Project",
                Start_Date = DateTime.Now,
                End_Date = DateTime.Now.AddMonths(2),
                Priority = 5
            };

            var result = _mapper.Map<boObject.Project>(entity);
            Assert.That(result.ProjectId == 101);
            Assert.That(result.ProjectName == "Project");
        }

        [Test]
        public void ProjectMaps_ProjectBusinessObjectToDbEntity()
        {
            var bo = new boObject.Project
            {
                ProjectName = "Test",
                Priority = 10
            };
            var result = _mapper.Map<dbEntity.Project>(bo);
            Assert.That(result.Priority == 10);
        }

        [Test]
        public void ProjectMaps_MapsTasksCount()
        {
            var entity = new dbEntity.Project
            {
                Project_Id = 101,
                Project_Name = "Project",
                Start_Date = DateTime.Now,
                End_Date = DateTime.Now.AddMonths(2),
                Priority = 5,
                Tasks = new List<dbEntity.Task> {
                    new dbEntity.Task { Task_Id = 2, Task_Name = "Task 2", Project_Id = 11, Status = false},
                    new dbEntity.Task { Task_Id = 3, Task_Name = "Task 3", Project_Id = 11, Status = true},
                    new dbEntity.Task { Task_Id = 4, Task_Name = "Task 4", Project_Id = 11, Status = true},
                }
            };
            var result = _mapper.Map<boObject.Project>(entity);

            Assert.That(result.ProjectId == 101);
            Assert.That(result.NoOfTasks == 3);
            Assert.That(result.NoOfCompletedTasks == 1);

        }

    }
}
